package com.anhpq.tino.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.anhpq.tino.data.dao.UserDao
import com.anhpq.tino.data.model.User

@Database(entities = [(User::class)], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}