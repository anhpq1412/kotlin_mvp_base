package com.anhpq.tino.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.anhpq.tino.utils.Define

@Entity(tableName = Define.TableUser.TABLE_NAME)
data class User(
    @ColumnInfo(name = Define.TableUser.UUID)
    var uuid: String,
    @ColumnInfo(name = Define.TableUser.NAME)
    var name: String,
    @ColumnInfo(name = Define.TableUser.PASS)
    var pass: Int
) {}