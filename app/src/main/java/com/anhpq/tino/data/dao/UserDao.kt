package com.anhpq.tino.data.dao

import androidx.room.*
import com.anhpq.tino.data.model.User
import com.anhpq.tino.utils.Define

@Dao
interface UserDao {

    @Query("SELECT count(*) FROM ${Define.TableUser.TABLE_NAME}")
    fun checkUser(): Int

    @Query("SELECT * FROM ${Define.TableUser.TABLE_NAME} WHERE ${Define.TableUser.UUID} = :uuid")
    fun getUser(uuid: String): User

    @Insert(onConflict = 1)
    fun insertUser(user: User)

    @Update(onConflict = 1)
    fun updatetUser(user: User)

    @Delete
    fun deleteUser(user: User)
}