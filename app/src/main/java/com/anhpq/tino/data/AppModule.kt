package com.anhpq.tino.data

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides

@Module
class AppModule(private val context: Context) {
    @Provides
    fun provideAppContext() = context

    @Provides
    fun provideAppDatabase(context: Context): AppDatabase =
        Room.databaseBuilder(context, AppDatabase::class.java, "taichinhcanhan").allowMainThreadQueries().build()

    @Provides
    fun provideToDoDao(database: AppDatabase) = database.userDao()
}