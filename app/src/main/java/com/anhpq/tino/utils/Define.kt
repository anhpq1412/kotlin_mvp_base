package com.anhpq.tino.utils

class Define {
    companion object {
        const val BASE_URL = "";
    }

    class TableUser {
        companion object {
            const val TABLE_NAME = "tb_user"
            const val UUID = "userUUID"
            const val NAME = "userName"
            const val PASS = "userPass"
        }
    }
}