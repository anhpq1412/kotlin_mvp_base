package com.anhpq.tino.ui.home

import com.anhpq.tino.base.BaseView

/**
 * Interface providing required method for a view displaying posts
 */
interface HomeView : BaseView {
    /**
     * Displays an error in the view
     * @param error the error to display in the view
     */
    fun showError(error: String)

    /**
     * Displays the loading indicator of the view
     */
    fun showLoading()

    /**
     * Hides the loading indicator of the view
     */
    fun hideLoading()
}