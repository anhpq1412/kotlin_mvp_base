package com.anhpq.tino.ui.home

import com.anhpq.tino.base.BasePresenter


/**
 * The Presenter that will present the Post view.
 * @param postView the Post view to be presented by the presenter
 * @property postApi the API interface implementation
 * @property context the context in which the application is running
 * @property subscription the subscription to the API call
 */
class HomePresenter(homeView: HomeView) : BasePresenter<HomeView>(homeView) {
    override fun onViewCreated() {
        super.onViewCreated()
    }

    override fun onViewDestroyed() {
        super.onViewDestroyed()
    }
}