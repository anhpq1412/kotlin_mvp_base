package com.anhpq.tino.ui.splash

import android.content.Intent
import android.os.Handler
import com.anhpq.tino.R
import com.anhpq.tino.base.BaseActivity
import com.anhpq.tino.ui.home.HomeActivity

class SplashActivity : BaseActivity<SplashPresenter>(), SplashView {
    override fun initContentView() {
        setContentView(R.layout.splash_view)
    }

    override fun instantiatePresenter(): SplashPresenter {
        return SplashPresenter(this)
    }

    override fun onResume() {
        super.onResume()

        presenter.checkUser()


        Handler().postDelayed({
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        },2000L)
    }
}