package com.anhpq.tino.ui.splash

import com.anhpq.tino.base.BasePresenter
import com.anhpq.tino.data.dao.UserDao
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SplashPresenter(splashView: SplashView) : BasePresenter<SplashView>(splashView) {

    @Inject
    private lateinit var  userDao: UserDao

    val compositeDisposable = CompositeDisposable()

    override fun onViewCreated() {
        super.onViewCreated()
    }

    override fun onViewDestroyed() {
        super.onViewDestroyed()
        compositeDisposable.dispose()
    }

    fun checkUser() {
        userDao.checkUser()
    }
}